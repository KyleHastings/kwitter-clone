import api from "../../utils/api";

// AUTH CONSTANTS
export const LOGIN = "AUTH/LOGIN";
export const LOGIN_SUCCESS = "AUTH/LOGIN_SUCCESS";
export const LOGIN_FAILURE = "AUTH/LOGIN_FAILURE";
export const LOGOUT = "AUTH/LOGOUT";
export const REGISTER = "AUTH/REGISTER";
export const REGISTER_FAILURE = "AUTH/REGISTER_FAILURE";
export const REGISTER_SUCCESS = "AUTH/REGISTER_SUCCESS";
export const POST_LIKES = "AUTH/POST_LIKES";
export const DELETE_LIKES = "AUTH/DELETE_LIKES";
export const GET_A_MESSAGE = "AUTH/GET_A_MESSAGE";
export const POST_A_MESSAGE = "AUTH/POST_A_MESSAGE";
export const DELETE_A_MESSAGE = "AUTH/DELETE_A_MESSAGE";

/*
 AUTH ACTIONS (this is a thunk....)
 THUNKS: --> https://github.com/reduxjs/redux-thunk#whats-a-thunk
 If you need access to your store you may call getState()
*/
export const login = (credentials) => async (dispatch, getState) => {
  try {
    dispatch({ type: LOGIN });
    const payload = await api.login(credentials);
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    // console.log({ result })
    dispatch({ type: LOGIN_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: LOGIN_FAILURE,
      payload: err.message,
    });
  }
};

export const logout = () => async (dispatch, getState) => {
  try {
    // We do not care about the result of logging out
    // as long as it succeeds
    await api.logout();
  } finally {
    /**
     * Let the reducer know that we are logged out
     */
    dispatch({ type: LOGOUT });
  }
};

export const register = (username, displayName, password) => async (
  dispatch,
  getState
) => {
  console.log("running");
  try {
    dispatch({ type: REGISTER });
    const payload = await api.register(username, displayName, password);
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    // console.log({ result })
    dispatch({ type: REGISTER_SUCCESS, payload });
  } catch (err) {
    dispatch({
      type: REGISTER_FAILURE,
      payload: err.message,
    });
  }
};

export const getAllMessages = () => async (dispatch, getState) => {
  try {
    const payload = await api.getAllMessages();
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    // console.log({ result })
    return payload;
  } catch (err) {}
};

export const getAMessage = (credentials) => async (dispatch, getState) => {
  try {
    const payload = await api.getAMessage(credentials);
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    // console.log({ result })
    dispatch({ type: GET_A_MESSAGE, payload });
  } catch (err) {}
};

export const deleteAMessage = (credentials) => async (dispatch, getState) => {
  try {
    const payload = await api.deleteAMessage(credentials);
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    // console.log({ result })
    dispatch({ type: DELETE_A_MESSAGE, payload });
  } catch (err) {}
};

export const postAMessage = (credentials) => async (dispatch, getState) => {
  try {
    const payload = await api.postAMessage(credentials);
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    // console.log({ result })
    dispatch({ type: POST_A_MESSAGE, payload });
  } catch (err) {}
};

export const postLikes = (credentials) => async (dispatch, getState) => {
  try {
    const payload = await api.postLikes(credentials);
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    // console.log({ result })
    dispatch({ type: POST_LIKES, payload });
  } catch (err) {}
};

export const deleteLikes = (credentials) => async (dispatch, getState) => {
  try {
    const payload = await api.deleteLikes(credentials);
    // ℹ️ℹ️This is how you woud debug the response to a requestℹ️ℹ️
    // console.log({ result })
    dispatch({ type: DELETE_LIKES, payload });
  } catch (err) {}
};

// END AUTH ACTIONS

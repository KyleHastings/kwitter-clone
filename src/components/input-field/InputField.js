import React from "react";
import "./InputField.css";
import TextField from "@material-ui/core/TextField";
import api from "../../utils/api"
class InputField extends React.Component {
    state = { 
        message: "" 
    };

  handleSubmit = e => {
    e.preventDefault();
    api.postAMessage(this.state);
    this.setState({ message: "" });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { loading } = this.props;
    return (
      <React.Fragment>
        {/* <form  onSubmit={this.handleSubmit}
            className="textArea"
            maxLength="150"
            placeholder="What's on your mind?"
            type="text"
            name="text"
            autoFocus
            required
            onChange={this.handleChange}
            value={this.state.text}
        /> */}
        <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              value={this.state.message}
              name="message"
              label="Tell us how you feel!"
              type="message"
              id="message"
              autoComplete="current-password"
              onChange = {this.handleChange}
            />  
        <button 
          type="submit" 
          disabled={loading}
          onClick={this.handleSubmit}
        >
            Post
        </button>
      </React.Fragment>
    );
  }
}

export default InputField;
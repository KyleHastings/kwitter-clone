import React, { Component, useEffect , useState} from "react";
//, { useState }
import Card from '@material-ui/core/Card';
import InputField from "../input-field/InputField";
import ProptTypes from "prop-types";
import { Loader } from "../loader";
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';


const useStyles = makeStyles({
  root: {
    minWidth: 275,
  },
  bullet: {
    display: 'inline-block',
    margin: '0 2px',
    transform: 'scale(0.8)',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
});


export const MessageFeed = ({loading, error, messages }) => {
  // Not to be confused with "this.setState" in classes\
  const classes = useStyles();
  const bull = <span className={classes.bullet}>•</span>;
  async function handleMessages(){
    // let tempMessages = (await getAllMessages()) 
    // tempMessages = {...tempMessages.messages}
    // console.log(tempMessages)

    // let tempMessages = [...request.messages]
    //   return (tempMessages)

    // return tempMessages
  }
  
  return (
    <React.Fragment>
      <InputField />
      <ul>{messages.map(message =>
      <Card className={classes.card}>
      <CardContent>
      <Typography className={classes.title} color="textPrimary" gutterBottom>
          {message.username}
        </Typography>
        <Typography className={classes.title} color="textSecondary" gutterBottom>
          {message.text}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small">Like</Button>
      </CardActions>
    </Card>
       )}
      </ul> 
      {/* {/* {loading && <Loader />}
      {error && <p style={{ color: "red" }}>{error.message}</p>} */}
      <div className="inputContainer">
        
      </div>
    </React.Fragment>
  );
};

MessageFeed.propTypes = {
  loading: ProptTypes.bool,
  error: ProptTypes.string,
};

import React from "react";
import { BrowserRouter, Switch } from "react-router-dom";
import { NotFoundScreen, LoginScreen } from "../../screens";
import { Register } from "../../screens/register/Register";
import Profile from "../../screens/profile/Profile";
import Home from "../../screens/home/Home";
import { ConnectedRoute } from "../connected-route/ConnectedRoute";

export const Navigation = () => (
  <BrowserRouter>
    <Switch>
      <ConnectedRoute
        exact
        path="/"
        redirectIfAuthenticated
        component={LoginScreen}
      />
      <ConnectedRoute
        exact
        path="/register"
        redirectIfAuthenticated
        component={Register}
      />
      <ConnectedRoute isProtected exact path="/messagefeed" component={Home} />
      <ConnectedRoute
        isProtected
        exact
        path="/profiles/:username"
        component={Profile}
      />
      <ConnectedRoute path="*" component={NotFoundScreen} />
    </Switch>
  </BrowserRouter>
);

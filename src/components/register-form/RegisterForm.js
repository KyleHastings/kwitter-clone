import React, { useState } from "react";
import ProptTypes from "prop-types";
import { Loader } from "../loader";
import { Link } from "react-router-dom";
// import Link from '@material-ui/core/Link';

import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
// import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from "@material-ui/core/Typography";
import { makeStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import Copyright from "../copyright/Copyright";

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    position: "absolute",
    width: "345px",
    height: "345px",
    left: "500px",
    top: "135px",
  },

  form: {
    width: "100%",
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

export const RegisterForm = ({ register, loading, error }) => {
  const [state, setState] = useState({
    username: "",
    displayName: "",
    password: "",
    firstName: "",
    lastName: "",
    emailAddress: "",
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    console.log("state: ", state);
    console.log("event.target: ", event.target);
    register(state.username, state.displayName, state.password);
    setState({
      username: "",
      displayName: "",
      password: "",
      firstName: "",
      lastName: "",
    });
    alert("account registered!");
  };

  const handleChange = (event) => {
    const inputName = event.target.name;
    const inputValue = event.target.value;
    setState((prevState) => ({ ...prevState, [inputName]: inputValue }));
  };

  const classes = useStyles();
  return (
    <React.Fragment>
      <Container component="main" maxWidth="s">
        <CssBaseline />

        <div className={classes.paper}>
          <Typography component="h3" variant="h5">
            Sign up
          </Typography>

          <form
            id="login-form"
            className={classes.Form}
            onSubmit={handleSubmit}
          >
            <Grid container spacing={2}>
              <Grid item xs={12} sm={6}>
                <TextField
                  type="text"
                  name="username"
                  value={state.firstName}
                  autoFocus
                  required
                  onChange={handleChange}
                  autoComplete="fname"
                  variant="outlined"
                  fullWidth
                  id="firstName"
                  name="firstName"
                  label="First Name"
                />
              </Grid>

              <Grid item xs={12} sm={6}>
                <TextField
                  type="text"
                  value={state.lastName}
                  required
                  onChange={handleChange}
                  variant="outlined"
                  fullWidth
                  id="lastName"
                  label="Last Name"
                  name="lastName"
                  autoComplete="lname"
                />
              </Grid>
              <Grid item xs={12}>
                <TextField
                  value={state.email}
                  required
                  onChange={handleChange}
                  variant="outlined"
                  fullWidth
                  id="username"
                  label="Username"
                  name="username"
                  autoComplete="username"
                />
              </Grid>

              <Grid item xs={12}>
                <TextField
                  variant="outlined"
                  required
                  fullWidth
                  name="password"
                  label="Password"
                  type="password"
                  id="password"
                  value={state.password}
                  autoComplete="current-password"
                  onChange={handleChange}
                />
              </Grid>
              <Grid item xs={12}>
                <FormControlLabel
                  control={
                    <Checkbox value="allowExtraEmails" color="primary" />
                  }
                  label="I want to receive the latest Blogs via email."
                />
              </Grid>
            </Grid>

            <Button
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              type="submit"
              disabled={loading}
              onClick={handleSubmit}
            >
              Register Account
            </Button>

            {/* make nav button to go directly to home?  make text field form?*/}

            <Grid container justify="flex-end">
              <Grid item>
                <Link
                  style={{ textDecoration: "inherit", color: "inherit" }}
                  to="/#"
                  variant="body2"
                >
                  Already have an account? Sign in
                </Link>
              </Grid>
            </Grid>
          </form>
          <Box mt={5}>
            <Copyright />
          </Box>
        </div>

        {loading && <Loader />}
        {error && <p style={{ color: "red" }}>{error.message}</p>}
      </Container>
    </React.Fragment>
  );
};

RegisterForm.propTypes = {
  loading: ProptTypes.bool,
  error: ProptTypes.string,
};

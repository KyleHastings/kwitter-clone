import React, { Component } from "react";
import { MenuContainer } from "../components";
import { MessageFeed } from "../components/message-feed/MessageFeed";
import { RegisterFormContainer } from "../components/register-form/index";
import { MessageFeedContainer } from "../components/message-feed";

export const MessagesScreen = () => (
  <>
    <MenuContainer />

    <MessageFeedContainer />
  </>
);

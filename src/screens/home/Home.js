import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { LoginFormContainer, MenuContainer } from "../../components";
import { getUsersAction } from "../../redux/actions/users";
import { MessageFeedContainer } from "../../components/message-feed";
import api from "../../utils/api";

class Home extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: null,
      messages: [],
    };
    this.handleMessages();
  }

  componentDidMount() {
    this.fetchUsersList();
  }

  fetchUsersList = () => {
    return api.getUsersList().then((result) => {
      this.props.dispatch(getUsersAction(result));
    });
  };

  async handleMessages() {
    let tempMessages = await api.getAllMessages();
    tempMessages = [...tempMessages.messages];
    console.log(tempMessages);
    this.setState({ messages: tempMessages });
    // let tempMessages = [...request.messages]
    //   return (tempMessages)

    return tempMessages;
  }

  render() {
    console.log(this.props.users);
    if (this.props.users) {
      return (
        <div>
          <MenuContainer username={this.props.auth.username} />
          {!this.props.auth.isAuthenticated && <LoginFormContainer />}
          <MessageFeedContainer messages={this.state.messages} />
          <div>
            <h4>Other Users</h4>
            {this.props.auth.isAuthenticated &&
              this.props.users &&
              this.props.users.users &&
              this.props.users.users.users &&
              this.props.users.users.users.map((user) => (
                <Link to={`/profiles/${user.username}/`}>
                  <div id={user.username}>{user.displayName}</div>
                </Link>
              ))}
          </div>
        </div>
      );
    }
    return <div className="loading_screen">{"Loading..."}</div>;
  }
}

function mapStateToProps(state) {
  return {
    users: state.users,
    auth: state.auth,
  };
}

export default connect(mapStateToProps)(Home);

import React, { Component } from "react";
import { connect } from "react-redux";
import { MenuContainer } from "../../components";
import { getProfileAction } from "../../redux/actions/users";
import { getPhotoAction } from "../../redux/actions/users";
import { updatePhotoAction } from "../../redux/actions/users";
import "../profile/profile.css";

import api from "../../utils/api";

class Profile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      users: null,
      picture: null,
      userName: null,
    };
  }

  componentWillMount() {
    this.fetchUser();
  }

  fetchUser = () => {
    let userName = this.props.match.params.username;
    this.setState({ userName });
    return api.getUser(userName).then((result) => {
      this.props.dispatch(getProfileAction(result));
    });
  };

  fetchUserPhoto = () => {
    let userName = this.props.match.params.username;
    return api.getPhoto(userName).then((result) => {
      this.props.dispatch(getPhotoAction(result));
    });
  };

  onImageChange = (e) => {
    const file = e.target.files[0];
    this.setState({
      photo: file,
    });
  };

  updatePhoto = (e) => {
    e.preventDefault();

    api
      .setUserPhoto(this.state.photo, this.props.match.params.username)
      .then((result) => {
        this.props.dispatch(updatePhotoAction(result));
      });
  };

  render() {
    return (
      <div>
        <MenuContainer username={this.props.auth.username} />
        <div className="profileHeader">
          <div className="headerDiv"></div>
          {this.props.profile &&
          this.props.profile.user &&
          this.props.profile.user.pictureLocation ? (
            <img
              className="photo"
              src={`https://kwitter-api.herokuapp.com${this.props.profile.user.pictureLocation}`}
              alt={"Profile pic"}
            />
          ) : (
            <img className="photo" src={require("./unnamed.png")} alt={""} />
          )}
        </div>
        <div className="infoDiv">
          {this.props.profile && this.props.profile.user && (
            <h1 id={this.props.profile.user.username}>
              {this.props.profile.user.displayName}
            </h1>
          )}
          <p>Indianapolis, IN</p>
          <p>{this.props.about}</p>
        </div>
        {this.props.match.params.username === this.props.auth.username ? (
          <div className="updatePhoto">
            {/* REFRESH THE PAGE AFTER SUBMITTING PHOTO */}

            <form class="form" onSubmit={this.updatePhoto}>
              <input
                type="file"
                name="picture"
                id="picture"
                onChange={this.onImageChange}
              ></input>
              <button type="submit">Update Photo</button>
              <p>Refresh page after submitting</p>
            </form>
          </div>
        ) : null}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    auth: state.auth,
    profile: state.users.profile,
    about:
      "'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Fringilla ut morbi tincidunt augue.'",
  };
}
export default connect(mapStateToProps)(Profile);
